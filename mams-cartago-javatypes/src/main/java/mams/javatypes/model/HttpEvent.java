package mams.javatypes.model;

import java.util.function.Function;

import mams.utils.HttpContext;
import mams.web.ResponseEntity;

public class HttpEvent {
    public HttpContext context;
    public Function<HttpContext, ResponseEntity> method;

    public HttpEvent(HttpContext context, Function<HttpContext, ResponseEntity> method) {
        this.context = context;
        this.method = method;
    }
}
