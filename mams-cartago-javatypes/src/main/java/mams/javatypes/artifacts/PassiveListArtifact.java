package mams.javatypes.artifacts;

import cartago.ARTIFACT_INFO;
import cartago.ArtifactId;
import cartago.LINK;
import cartago.OPERATION;
import cartago.OUTPORT;
import cartago.Op;
import cartago.OpFeedbackParam;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.ResourceArtifact;
import mams.utils.CartagoBackend;
import mams.utils.Utils;
import mams.web.Handler;
import mams.web.ResponseEntity;



@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveListArtifact extends PassiveArtifact {
    {
        set(HttpMethod.GET, context -> {
            try {
                ResourceArtifact[] children = getChildren();
                Object[] array = new Object[children.length];
                int i = 0;
                for (ResourceArtifact artifact : children) {
                    array[i++] =((TypedResourceArtifact) artifact).getObject();
                }
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString(array));
                
            } catch(Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
        set(HttpMethod.POST, context -> {
            try {
                Object data = mapper.readValue(Utils.getBody(context.getRequest()), Class.forName(className));
                if (data == null) System.exit(0);
                String name = Utils.getIdentifier(className, data);

                OpFeedbackParam<ArtifactId> id = new OpFeedbackParam<ArtifactId>();
                String qname = getId().getName()+"-"+name;
                CartagoBackend.getInstance().
                    doAction(new Op("makeArtifact", qname, "mams.javatypes.artifacts.PassiveListItemArtifact", new Object[] {name, className, data}, id));
    
                CartagoBackend.getInstance().doAction(new Op("linkArtifacts", id.get(), "out-1", getId()));
                CartagoBackend.getInstance().doAction(id.get(), new Op("createRoute"));
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK);
            } catch(Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
    }

    @OPERATION
	void init(final String name, final String className) {
        this.name = name;
        this.className = className;
    }

    protected int size() {
        return this.handler.getLinks().size();
    }

    @OPERATION
    public void size(final OpFeedbackParam<Integer> size) {
        size.set(size());
    }

    @LINK
	public void attach(String id, Handler childHandler, OpFeedbackParam<String> baseUri) {
        super.attach(id, childHandler, baseUri);
        this.signal("listItemArtifactCreated", id, className);
    }
}


