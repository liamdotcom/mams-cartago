package mams.javatypes.artifacts;

import java.lang.reflect.Field;

import cartago.ARTIFACT_INFO;
import cartago.OPERATION;
import cartago.OUTPORT;
import cartago.Op;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.utils.CartagoBackend;
import mams.utils.Identifier;
import mams.utils.Utils;
import mams.web.ResponseEntity;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveListItemArtifact extends PassiveItemArtifact {
    {
        set(HttpMethod.DELETE, context -> {
            try {
                CartagoBackend.getInstance().doAction(getId(), new Op("destroyArtifact"));
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString(getObject()));
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
    }

    @OPERATION public void replace(Object data) throws Exception {
        Class<?> myClass = Class.forName(className);

        // Check thgat the identifier has not been modified
        Field[] fields = myClass.getFields();
        for (Field field : fields) {
            Object id = getProperty(field.getName());
            if (field.isAnnotationPresent(Identifier.class) && Utils.isInvalidIdentifier(field, data, id)) {
                throw new IllegalArgumentException("Illegal attempt to change the modifier of an existing resource: " + id + " to: " + field.get(data));
            }
        }

        // Update the properties
        for (Field field : fields) {
            if (field.get(data) != null) 
                this.updateObsProperty(field.getName(), field.get(data));
        }

        signal("replaced", data);
    }

    @OPERATION public void updated(Object data) throws Exception {
        Class<?> myClass = Class.forName(className);

        // Check thgat the identifier has not been modified
        Field[] fields = myClass.getFields();
        for (Field field : fields) {
            Object id = getProperty(field.getName());
            if (field.isAnnotationPresent(Identifier.class) && Utils.isInvalidIdentifier(field, data, id)) {
                throw new IllegalArgumentException("Illegal attempt to change the modifier of an existing resource: " + id + " to: " + field.get(data));
            }
        }

        // Update the properties
        for (Field field : fields) {
            if (field.get(data) != null) 
                this.updateObsProperty(field.getName(), field.get(data));
        }

        signal("updated", data);
    }
}
