package mams.javatypes.artifacts;

import java.lang.reflect.Field;

import com.fasterxml.jackson.databind.ObjectMapper;

import cartago.OPERATION;
import cartago.OpFeedbackParam;
import cartago.OperationException;
import mams.artifacts.ResourceArtifact;
import mams.utils.DefaultValue;

public abstract class TypedResourceArtifact extends ResourceArtifact {
    protected static ObjectMapper mapper = new ObjectMapper();
    protected String className;

    protected Object getObject() {
        try {
            Class<?> myClass = Class.forName(className);
            
            Object object = myClass.getConstructor().newInstance();
            Field[] fields = myClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                field.set(object, this.getObsProperty(field.getName()).getValue());
            }
            return object;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }  
    
	public ResourceArtifact[] getChildren() {
		return handler.getChildren();
	}

    @OPERATION void type(OpFeedbackParam<String> type) {
        type.set(className);
	}

	@OPERATION void init(String name, String className) {
        this.name = name;
        this.className = className;
         
        try {
            Class<?> myClass = Class.forName(className);
            Field[] fields = myClass.getFields();
            for (Field field : fields) {
                this.defineObsProperty(field.getName(), DefaultValue.forClass(field.getType()));
            }
        } catch (Exception e) {
            System.out.println("Failed to create DataArtifact for: " + className);
            e.printStackTrace();
        }
    }

    @OPERATION void init(String name, String className, Object data) {
        this.name = name;
        this.className = className;
        try {
            Class<?> myClass = Class.forName(className);
            Field[] fields = myClass.getFields();
            for (Field field : fields) {
                this.defineObsProperty(field.getName(), field.get(data));
            }
        } catch (Exception e) {
            System.out.println("Failed to create ItemArtifact for: " + className);
            e.printStackTrace();
        }
    }
    
    @OPERATION void getStringProperty(String key, OpFeedbackParam<String> value) {
        value.set(this.getObsProperty(key).stringValue());
    }

    @OPERATION void setStringProperty(String key, String value) {
        this.updateObsProperty(key, value);
    }

    @OPERATION public void replace(Object data) throws Exception {
        Class<?> myClass = Class.forName(className);

        // Update the properties
        Field[] fields = myClass.getFields();
        for (Field field : fields) {
            if (field.get(data) != null) 
                this.updateObsProperty(field.getName(), field.get(data));
        }
        signal("replaced", data);
    }

    @OPERATION public void update(Object data) throws Exception {
        // Convert the content
        Class<?> myClass = Class.forName(className);

        // Update the properties
        Field[] fields = myClass.getFields();
        for (Field field : fields) {
            if (field.get(data) != null) 
                this.updateObsProperty(field.getName(), field.get(data));
        }
        signal("patchedResource", data);
    }

    @OPERATION public void getObject(OpFeedbackParam<Object> param) {
        param.set(this.getObject());
    }

    @OPERATION void destroyArtifact(){
        try {
			execLinkedOp("out-1", "detach", this.name);
		} catch (OperationException e) {
			e.printStackTrace();
		}
    }
}
