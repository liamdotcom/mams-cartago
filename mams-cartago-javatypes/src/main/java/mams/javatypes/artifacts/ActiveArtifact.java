package mams.javatypes.artifacts;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

import cartago.OPERATION;
import cartago.Op;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.javatypes.model.HttpEvent;
import mams.utils.CartagoBackend;
import mams.utils.HttpContext;
import mams.web.ResponseEntity;
import mams.web.WebServer;

public class ActiveArtifact extends TypedResourceArtifact {
    private static int INDEX = 0;
    private Map<Integer, HttpEvent> events = new TreeMap<Integer, HttpEvent>();
    private Map<HttpMethod, Function<HttpContext, ResponseEntity>> methods = new HashMap<>();

    {
        set(HttpMethod.OPTIONS, context -> {
            StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (HttpMethod method : methods.keySet()) {
                if (first) first=false; else builder.append(",");
                builder.append(method.asciiName());
            }
            return new ResponseEntity(HttpResponseStatus.NO_CONTENT).header("Allow", builder.toString());
        });
        set(HttpMethod.GET, context -> {
            try {
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString(getObject()));
            } catch(Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });

    }

    protected void set(HttpMethod method, Function<HttpContext,ResponseEntity> context) {
        methods.put(method, context);
    }

    @OPERATION public void accept(int id) {
        HttpEvent event = events.remove(id);
        
        WebServer.writeResponse(
                event.context.getCtx(),
                event.context.getRequest(),
                event.method.apply(event.context));
    }

    @OPERATION public void reject(int id) {
        HttpEvent event = events.remove(id);

        WebServer.writeErrorResponse(
                event.context.getCtx(),
                event.context.getRequest(),
                HttpResponseStatus.FORBIDDEN);
    }

    @OPERATION public void httpEvent(int id, String method) {
        signal("httpEvent", id, method);
    }

    public boolean handle(ChannelHandlerContext ctx, FullHttpRequest request) {
        Function<HttpContext, ResponseEntity> method = methods.get(request.method());

        if (method == null) {
            WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.METHOD_NOT_ALLOWED);
        } else {
            int index = INDEX++;
            events.put(index, new HttpEvent(new HttpContext(request, ctx), method));
            try {
                CartagoBackend
                    .getInstance()
                    .doAction(getId(), new Op("httpEvent", index, request.method().toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }    
}