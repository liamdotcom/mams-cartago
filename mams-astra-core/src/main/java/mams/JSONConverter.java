package mams;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import astra.core.Module;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;
import astra.type.Type;

/**
 * Example Usage:
 *
 * - JSON:
 *  { "name" : "rem", "address" : { "street": "rehoboth place"} , "items" : ["one", "two", "three"] }
 *
 * - ASTRA:
 *
 * import com.fasterxml.jackson.databind.JsonNode;
 *
 * agent Example {
 *   module JSONConverter converter;
 *   ...
 *   rule +!example(string json) {
 *     JsonNode node = converter.parse(json);
 *     string name = converter.valueAsString(node, "/name");
 *     string street = converter.valueAsString(node, "/address/street");
 *     int length = converter.length(node, "/address/items");
 *     string itemTwo = converter.valueAsString(node, "/address/items[1]");
 *   }
 * }
 */
public class JSONConverter extends Module {

    ObjectMapper objectMapper = new ObjectMapper();

    @TERM
    public JsonNode parse(String json){
        try{
            JsonNode node = objectMapper.readTree(json);
            // System.out.println("NODE: "+node);
            return node;
        } catch(JsonParseException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    //JsonNode node = json.parse(json-string)
    //json.valueAsString(node, )

    @TERM
    public String valueAsString(JsonNode node, String path){
        // System.out.println("in node: " + node);
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asText();
    }

    @FORMULA
    public Formula compare(JsonNode node, String path, String value) {
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asText().equals(value) ? Predicate.TRUE:Predicate.FALSE;
    }

    @FORMULA
    public Formula compare(JsonNode node, String path, double value) {
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asDouble() == value ? Predicate.TRUE:Predicate.FALSE;
    }

    @FORMULA
    public Formula compare(JsonNode node, String path, int value) {
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asInt() == value ? Predicate.TRUE:Predicate.FALSE;
    }

    @FORMULA
    public Formula compare(JsonNode node, String path, long value) {
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asLong() == value ? Predicate.TRUE:Predicate.FALSE;
    }

    @TERM
    public double valueAsDouble(JsonNode node, String path){
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asDouble();
    }

    @TERM
    public long valueAsLong(JsonNode node, String path){
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asLong();
    }

    @TERM
    public int length(JsonNode node, String path) {
        JsonNode jsonNode = getNode(node, path);
        // System.out.println("jsonNode:" + jsonNode);
        if (!jsonNode.isArray()) {
            throw new RuntimeException("Attempt to read length of JSON node that is not a list");
        }
        return jsonNode.size();
    }

    @TERM
    public JsonNode getNode(JsonNode node, String path){
        // System.out.println("node: " + node);
        // System.out.println("path: " + path);
        String split[] = path.split("/");
        JsonNode temp = node;
        for(String s: split){
            if(s.isEmpty()) continue;
            int index  = s.indexOf("[");

            if(index == -1){
                temp = temp.get(s);
            }else{
                if(index > 0){
                    String segment = s.substring(0, index);
                    temp = temp.get(segment); 
                }
                String rank = s.substring(index+1, s.length()-1);
                // System.out.println("rank: " + rank);
                // System.out.println("temp: " + temp);
                temp = temp.get(Integer.parseInt(rank));
                // System.out.println("temp: " + temp);
            }
        }
        return temp;
    }

    @TERM
    public String convertKeyValueToString(String key, String value){
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put(key, value);
        String json;
        try {
            json = objectMapper.writeValueAsString(objectNode);
            return json;
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    @TERM
    public Funct toRawFunct(String predicate, String json) {
        try {
            JsonNode tree = objectMapper.readTree(json);

            // Calculate the number of non_hal fields
            Term[] terms = new Term[tree.size()];

            int i = 0;
            Iterator<Entry<String, JsonNode>> it = tree.fields();
            while (it.hasNext()) {
                Entry<String, JsonNode> field = it.next();
                if (field.getValue().isObject()) {
                    terms[i++] = new Funct(field.getKey(), new Term[] {createTerm(field.getKey(), field.getValue()) });;
                } else {
                    terms[i++] = new Funct(field.getKey(), new Term[] {createTerm(field.getValue()) });;
                }
            }
            return new Funct(predicate, terms);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Term createTerm(JsonNode node) {
        if (node.isArray()) {
            ListTerm items = new ListTerm();
            Iterator<JsonNode> it = node.elements();
            while (it.hasNext()) {
                items.add(createTerm(it.next()));
            }
            return items;
        } else if (node.size() == 0) {
            if (node.isLong()) {
                return Primitive.newPrimitive(node.asLong());
            } else if (node.isInt()) {
                return Primitive.newPrimitive(node.asInt());
            } else if (node.isDouble()) {
                return Primitive.newPrimitive(node.asDouble());
            } else if (node.isFloat()) {
                return Primitive.newPrimitive(node.floatValue());
            } else if (node.isBoolean()) {
                return Primitive.newPrimitive(node.asBoolean());
            } else if (node.isTextual()) {
                return Primitive.newPrimitive(node.asText());
            }
        }
        throw new RuntimeException("[JSONConverter] Could not convert: " + node);
    }

    private Term createTerm(String key, JsonNode node) {
        Term[] terms = null;
        if (node.size() == 0) {
            terms = new Term[1];
            if (node.isLong()) {
                terms[0] = Primitive.newPrimitive(node.asLong());
            } else if (node.isInt()) {
                terms[0] = Primitive.newPrimitive(node.asInt());
            } else if (node.isDouble()) {
                terms[0] = Primitive.newPrimitive(node.asDouble());
            } else if (node.isFloat()) {
                terms[0] = Primitive.newPrimitive(node.floatValue());
            } else if (node.isBoolean()) {
                terms[0] = Primitive.newPrimitive(node.asBoolean());
            } else if (node.isTextual()) {
                terms[0] = Primitive.newPrimitive(node.asText());
            } else if (node.isArray()) {
                System.out.println("Converting array: " + key);
                ListTerm items = new ListTerm();
                Iterator<Entry<String, JsonNode>> it = node.fields();
                while (it.hasNext()) {
                    Entry<String, JsonNode> field = it.next();
                    items.add(createTerm(field.getKey(), field.getValue()));
                }
                System.out.println("List: " + items);
                return items;
            } else {
                throw new RuntimeException("[HALConverter] Could not convert: " + node);
            }
        } else {
            terms = new Term[node.size()];
            int i = 0;
            Iterator<Entry<String, JsonNode>> it = node.fields();
            while (it.hasNext()) {
                Entry<String, JsonNode> field = it.next();
                terms[i++] = createTerm(field.getKey(), field.getValue());
            }
        }

        return new Funct(key, terms);
    }

    /**
     * Outer function name is stripped for JSON generation
     * 
     * @param function
     * @return
     */
    @TERM
    public String toJsonString(Funct function) {
        try {
            return objectMapper.writeValueAsString(createJson(function));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
    
    private ObjectNode createJson(Funct function) {
        ObjectNode node  = objectMapper.createObjectNode();
        for (Term term : function.terms()) {
            if (Funct.class.isInstance(term)) {
                Funct funct = (Funct) term;
                Term value = funct.termAt(0);
                if (Funct.class.isInstance(value)) {
                    node.set(funct.functor(), createJson((Funct) value));
                } else if (ListTerm.class.isInstance(value)) {
                    node.set(funct.functor(), createJson((ListTerm) value));
                } else if (Primitive.class.isInstance(value)) {
                    addPrimitiveField(node, funct.functor(), value);
                }
            }
        }
        return node;
    }

    private ArrayNode createJson(ListTerm list) {
        ArrayNode node = objectMapper.createArrayNode();
        for (Term term : list) {
            if (Funct.class.isInstance(term)) {
                node.add(createJson((Funct) term));
            } else if (Primitive.class.isInstance(term)) {
                addPrimitive(node, term);
            }
        }
        return node;
    }

    @SuppressWarnings("unchecked")
    private void addPrimitive(ArrayNode node, Term value) {
        if (value.type().equals(Type.BOOLEAN)) {
            node.add(((Primitive<Boolean>) value).value());
        } else if (value.type().equals(Type.CHAR)) {
            node.add(((Primitive<Character>) value).value().toString());
        } else if (value.type().equals(Type.DOUBLE)) {
            node.add(((Primitive<Double>) value).value());
        } else if (value.type().equals(Type.FLOAT)) {
            node.add(((Primitive<Float>) value).value());
        } else if (value.type().equals(Type.INTEGER)) {
            node.add(((Primitive<Integer>) value).value());
        } else if (value.type().equals(Type.LONG)) {
            node.add(((Primitive<Long>) value).value());
        } else if (value.type().equals(Type.STRING)) {
            node.add(((Primitive<String>) value).value());
        } else {
            throw new RuntimeException("Unknown type: " + value.type());
        }
    }

    @SuppressWarnings("unchecked")
    private void addPrimitiveField(ObjectNode node, String key, Term value) {
        if (value.type().equals(Type.BOOLEAN)) {
            node.put(key, ((Primitive<Boolean>) value).value());
        } else if (value.type().equals(Type.CHAR)) {
            node.put(key, ((Primitive<Character>) value).value().toString());
        } else if (value.type().equals(Type.DOUBLE)) {
            node.put(key, ((Primitive<Double>) value).value());
        } else if (value.type().equals(Type.FLOAT)) {
            node.put(key, ((Primitive<Float>) value).value());
        } else if (value.type().equals(Type.INTEGER)) {
            node.put(key, ((Primitive<Integer>) value).value());
        } else if (value.type().equals(Type.LONG)) {
            node.put(key, ((Primitive<Long>) value).value());
        } else if (value.type().equals(Type.STRING)) {
            node.put(key, ((Primitive<String>) value).value());
        } else {
            throw new RuntimeException("Unknown type: " + value.type());
        }
    }

    @TERM public ListTerm getFields(JsonNode node) {
        ListTerm list = new ListTerm();
        Iterator<String> it = node.fieldNames();
        while (it.hasNext()) {
            list.add(Primitive.newPrimitive(it.next()));
        }
        return list;
    }

}
