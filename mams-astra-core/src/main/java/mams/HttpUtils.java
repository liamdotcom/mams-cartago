package mams;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Map.Entry;

import astra.core.Module;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;
import mams.web.HttpRequest;
import mams.web.HttpRequest.HttpRequestBuilder;
import mams.web.HttpResponse;

public class HttpUtils extends Module {

    @TERM
    public HttpRequestBuilder createHttpRequestBuilder() {
        return HttpRequest.newBuilder();
    }

    @TERM
    public HttpRequest get(String uri, ListTerm headers) {
        HttpRequestBuilder builder = HttpRequest.newBuilder();
        try {
            builder.method(HttpRequest.GET).uri(new URI(uri));
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to construct HttpRequest", e);
        }
        headers(builder, headers);
        return builder.build();
    }

    @TERM
    public HttpRequest get(String uri) {
        HttpRequestBuilder builder = HttpRequest.newBuilder();
        try {
            builder.method(HttpRequest.GET).uri(new URI(uri));
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to construct HttpRequest", e);
        }
        return builder.build();
    }

    @TERM
    public HttpRequest delete(String uri, ListTerm headers) {
        HttpRequestBuilder builder = HttpRequest.newBuilder();
        try {
            builder.method(HttpRequest.DELETE).uri(new URI(uri));
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to construct HttpRequest", e);
        }
        headers(builder, headers);
        return builder.build();
    }

    @TERM
    public HttpRequest delete(String uri) {
        HttpRequestBuilder builder = HttpRequest.newBuilder();
        try {
            builder.method(HttpRequest.DELETE).uri(new URI(uri));
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to construct HttpRequest", e);
        }
        return builder.build();
    }

    @TERM
    public HttpRequest head(String uri) {
        HttpRequestBuilder builder = HttpRequest.newBuilder();
        try {
            builder.method(HttpRequest.HEAD).uri(new URI(uri));
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to construct HttpRequest", e);
        }
        return builder.build();
    }

    @TERM
    public HttpRequest options(String uri) {
        HttpRequestBuilder builder = HttpRequest.newBuilder();
        try {
            builder.method(HttpRequest.OPTIONS).uri(new URI(uri));
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to construct HttpRequest", e);
        }
        return builder.build();
    }

    @TERM
    public HttpRequest post(String uri, ListTerm headers, String body) {
        return createHttpRequest(HttpRequest.POST, uri, headers, body);
    }

    @TERM
    public HttpRequest put(String uri, ListTerm headers, String body) {
        return createHttpRequest(HttpRequest.PUT, uri, headers, body);
    }

    @TERM
    public HttpRequest patch(String uri, ListTerm headers, String body) {
        return createHttpRequest(HttpRequest.PATCH, uri, headers, body);
    }

    private HttpRequest createHttpRequest(String method, String uri, ListTerm headers, String body) {
        HttpRequestBuilder builder = HttpRequest.newBuilder();
        try {
            builder.method(method).uri(new URI(uri)).body(HttpRequest.BodyPublishers.ofString(body));
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to construct HttpRequest", e);
        }
        headers(builder, headers);
        return builder.build();
    }

    @ACTION
    public boolean uri(HttpRequestBuilder builder, String uri) {
        try {
            builder.uri(new URI(uri));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    @ACTION
    public boolean method(HttpRequestBuilder builder, String method) {
        builder.method(method);
        return true;
    }
    
    @ACTION
    public boolean header(HttpRequestBuilder builder, String key, String value) {
        builder.header(key,value);
        return true;
    }

    @ACTION
    public boolean headers(HttpRequestBuilder builder, ListTerm headers) {
        for (Term term : headers) {
            Funct header = (Funct) term;

            builder.header(
                ((Primitive<?>) header.termAt(0)).value().toString(),
                ((Primitive<?>) header.termAt(1)).value().toString()
            );
        }
        return true;
    }

    @ACTION
    public boolean bodyAsString(HttpRequestBuilder builder, String body) {
        builder.body(HttpRequest.BodyPublishers.ofString(body));
        return true;
    }
   
    @TERM
    public HttpRequest build(HttpRequestBuilder builder) {
        return builder.build();
    }

    @TERM
    public int code(HttpResponse response) {
        return response.code();
    }

    @TERM
    public String header(HttpResponse response, String key) {
        return response.headers().get(key);
    }

    @TERM
    public String bodyAsString(HttpResponse response) {
        return new String(response.body(), StandardCharsets.UTF_8);
    }

    @FORMULA
    public Formula hasCode(HttpResponse response, int code) {
        return response.code() == code ? Predicate.TRUE:Predicate.FALSE;
    }

    @ACTION
    public boolean dumpHeaders(HttpResponse response) {
        for (Entry<String, String> header:response.headers().entrySet()) {
            System.out.println(header.getKey() + "->"+header.getValue());
        }
        return true;
    }
}
