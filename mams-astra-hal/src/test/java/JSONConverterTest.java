import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.fasterxml.jackson.databind.JsonNode;

import org.junit.Test;

import mams.JSONConverter;

public class JSONConverterTest {
    private JSONConverter converter = new JSONConverter();


    @Test
    public void stringFieldTest(){
        String json =  "{\"param\":\"value\"}";
        JsonNode node = converter.parse(json);
        assertNotNull(node);
        assertEquals("value", converter.valueAsString(node, "/param"));
    }

    @Test
    public void arrayTest(){
        String json = "[\"param\",\"value\"]";
        JsonNode node = converter.parse(json);
        assertNotNull(node);
        assertEquals("value", converter.valueAsString(node, "[1]"));
    }


    @Test
    public void arrayObjectTest(){
        String json = "[{\"param\":\"value\"}]";
        JsonNode node = converter.parse(json);
        assertNotNull(node);
        assertEquals("value", converter.valueAsString(node, "[0]/param"));
    }
    // @Test
    // public void oneFieldTest() {
    //     Funct funct = hal.toRawFunct("test", "{\"param\":\"value\"}");
    //     assertTrue("Failed String Comparison: " + funct.toString(), funct.toString().equals("test(param(\"value\"))"));
    //     String param = getFunct(funct, 0).functor();
    //     Object value = getPrimitive(getFunct(funct, 0), 0).value();
    //     assertTrue("Incorrect Param: " + param, param.equals("param"));
    //     assertTrue("Incorrect Value: " + value, value.equals("value"));
    // }

    // @Test
    // public void twoFieldTest() {
    //     Funct funct = hal.toRawFunct("test", "{\"param\":\"value\", \"param2\":\"value2\"}");
    //     assertTrue("Failed String Comparison: " + funct.toString(), funct.toString().equals("test(param(\"value\"),param2(\"value2\"))"));
    //     String param = getFunct(funct, 0).functor();
    //     Object value = getPrimitive(getFunct(funct, 0), 0).value();
    //     String param2 = getFunct(funct, 1).functor();
    //     Object value2 = getPrimitive(getFunct(funct, 1), 0).value();
    //     assertTrue("Incorrect Param: " + param, param.equals("param"));
    //     assertTrue("Incorrect Value: " + value, value.equals("value"));
    //     assertTrue("Incorrect Param 2: " + param2, param2.equals("param2"));
    //     assertTrue("Incorrect Value 2: " + value2, value2.equals("value2"));
    // }

    // @Test
    // public void listTest() {
    //     Funct funct = hal.toRawFunct("test", "{\"param\":[\"value\", \"value2\"]}");
    //     assertTrue("Failed String Comparison: " + funct.toString(), funct.toString().equals("test(param([\"value\",\"value2\"]))"));
    //     String param = getFunct(funct,0).functor();
    //     ListTerm list = getList(getFunct(funct, 0), 0);
    //     assertTrue("Incorrect Param: " + param, param.equals("param"));
    //     assertTrue("Incorrect List Size: " + list, list.size() == 2);
    // }

    // @Test
    // public void objectFieldTest() {
    //     Funct funct = hal.toRawFunct("test", "{\"field\": { \"param\":\"value\"}}");
    //     assertTrue("Failed String Comparison: " + funct.toString(), funct.toString().equals("test(field(field(param(\"value\"))))"));
    // }

}