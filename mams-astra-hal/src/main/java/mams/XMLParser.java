package mams;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XMLParser {

    public static Document convertStringToXMLDocument(String xmlString){
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        
        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try{
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
            
            //Parse the content to Document object
            Document doc = (Document) builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getString(String tagName, Element element) {
        NodeList list = element.getElementsByTagName(tagName);
        if (list != null && list.getLength() > 0) {
            NodeList subList = list.item(0).getChildNodes();

            if (subList != null && subList.getLength() > 0) {
                return subList.item(0).getNodeValue();
            }
        }

        return null;
    }
    
}
