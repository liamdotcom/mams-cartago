package mams;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import astra.core.Module;

public class JSONBuilder extends Module {
    private ObjectMapper mapper = new ObjectMapper();
    
    @TERM
    public JsonNode createObject(JsonNode node, String key) {
        JsonNode newNode = mapper.createObjectNode();
        ((ObjectNode) node).set(key, newNode);
        return newNode;
    }

    @TERM
    public JsonNode createObject() {
        return mapper.createObjectNode();
    }

    @TERM
    public JsonNode createArray() {
        return mapper.createArrayNode();
    }

    @TERM
    public JsonNode createArray(JsonNode node, String key) {
        JsonNode newNode = mapper.createArrayNode();
        ((ObjectNode) node).set(key, newNode);
        return newNode;
    }

    @ACTION
    public boolean addProperty(JsonNode node, String key, String value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean addProperty(JsonNode node, String key, Long value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean addProperty(JsonNode node, String key, int value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean addProperty(JsonNode node, String key, long value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean addProperty(JsonNode node, String key, float value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean addProperty(JsonNode node, String key, double value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean setProperty(JsonNode node, String key, String value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean setProperty(JsonNode node, String key, Long value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean setProperty(JsonNode node, String key, int value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean setProperty(JsonNode node, String key, long value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean setProperty(JsonNode node, String key, float value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @ACTION
    public boolean setProperty(JsonNode node, String key, double value) {
        ((ObjectNode) node).put(key, value);
        return true;        
    }
    
    @TERM
    public String toJsonString(JsonNode node) {
        try {
            return mapper.writeValueAsString(node);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to convert node to string", e);
        }
    }
}
