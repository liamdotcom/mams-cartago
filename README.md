# MAMS: Multi-Agent MicroServices

MAMS is an architectural style for integrating multi-agent systems into Microservices architectures. It achieves this by modelling agents as entities that have hypermedia bodies that are exposed as REST APIs. This provides a standard REST API that plain-old microservices can through.

MAMS Agents are also imbued with the ability to use REST to interact with plain-old microservices, ensuring that any MAMS deployment can be seemlessly integrated within a Microservices architecture without the need to learn any agent technologies or concepts (unless you are implementing the MAMS part).

An overview of MAMS can be found in a paper we presented at the [HypermediaMAS workshop in 2019](http://193.1.133.232/pubs/Collier2019.pdf).

This repository contains the code for the second incarnation of MAMS, which has been built on top of the [CArtAgO](http://cartago.sourceforge.net/) framework.  Details of this version were presented in the [Engineering MAS workshop in 2020](https://drive.google.com/file/d/1srjfgsRZ75i3oiWfZLWOXuA-YACLxuhk/view).

While this version of MAMS can be used by any agent programming framework that can work with [CArtAgO](http://cartago.sourceforge.net/), we have built a prototype integration for the [ASTRA Programming Language](https://gitlab.com/astra-language/astra-core).

# Using MAMS

All MAMS code is released as [Maven](https://maven.apache.org) artifacts hosted in the [MAMS Gitlab Package Repository](https://gitlab.com/mams-ucd/mams-cartago/-/packages). Some example MAMS programs written using ASTRA can be found [here](https://gitlab.com/mams-ucd/examples).
