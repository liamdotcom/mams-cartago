package mams.serializer;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import mams.data.Application;

public class ApplicationsJsonDeserializer extends JsonDeserializer<List<Application>> {
    
    @Override
    public List<Application> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        InnerItems innerItems = jp.readValueAs(InnerItems.class);

        return null;
    }

    private static class InnerItems {
        public List<Application> elements;
    }
}
