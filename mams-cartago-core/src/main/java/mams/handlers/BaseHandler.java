package mams.handlers;

import java.util.Map;
import java.util.TreeMap;

import mams.web.Path;
import mams.web.Handler;
import mams.web.WebServer;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;

public class BaseHandler implements Handler {
    private String name;
    private Map<String, Handler> routes = new TreeMap<String, Handler>();
    
    public BaseHandler(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void addRoute(String id, Handler handler) {
        routes.put(id, handler);
    }

    public void deleteRoute(String id){
        routes.remove(id);
    }


	public void handle(ChannelHandlerContext ctx, FullHttpRequest request, Path path) throws Exception {        
        if (path.length() == 0) {
            WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.FORBIDDEN);
        }  else {
            String prefix = path.prefix();
            // System.out.println("Routes: " + routes.keySet());
            // System.out.println("Classes: " + routes.values());
            // System.out.println("Prefix: " + prefix);
            Handler handler = routes.get(prefix);
            if (handler == null) {
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.NOT_FOUND);
            } else {
                handler.handle(ctx, request, path);
            }
        }
	}
}

