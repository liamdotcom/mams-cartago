package mams.content;

public class ContentDecodeException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = -2678745763575619739L;

    public ContentDecodeException(String msg, Throwable th) {
        super(msg, th);
    }

}
