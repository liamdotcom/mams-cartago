
package mams.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "renewalIntervalInSecs",
    "durationInSecs",
    "registrationTimestamp",
    "lastRenewalTimestamp",
    "evictionTimestamp",
    "serviceUpTimestamp"
})
public class LeaseInfo implements Serializable
{

    @JsonProperty("renewalIntervalInSecs")
    private Integer renewalIntervalInSecs;
    @JsonProperty("durationInSecs")
    private Integer durationInSecs;
    @JsonProperty("registrationTimestamp")
    private Long registrationTimestamp;
    @JsonProperty("lastRenewalTimestamp")
    private Long lastRenewalTimestamp;
    @JsonProperty("evictionTimestamp")
    private Integer evictionTimestamp;
    @JsonProperty("serviceUpTimestamp")
    private Long serviceUpTimestamp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -8738108721517560410L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public LeaseInfo() {
    }

    /**
     * 
     * @param renewalIntervalInSecs
     * @param registrationTimestamp
     * @param evictionTimestamp
     * @param lastRenewalTimestamp
     * @param durationInSecs
     * @param serviceUpTimestamp
     */
    public LeaseInfo(Integer renewalIntervalInSecs, Integer durationInSecs, Long registrationTimestamp, Long lastRenewalTimestamp, Integer evictionTimestamp, Long serviceUpTimestamp) {
        super();
        this.renewalIntervalInSecs = renewalIntervalInSecs;
        this.durationInSecs = durationInSecs;
        this.registrationTimestamp = registrationTimestamp;
        this.lastRenewalTimestamp = lastRenewalTimestamp;
        this.evictionTimestamp = evictionTimestamp;
        this.serviceUpTimestamp = serviceUpTimestamp;
    }

    @JsonProperty("renewalIntervalInSecs")
    public Integer getRenewalIntervalInSecs() {
        return renewalIntervalInSecs;
    }

    @JsonProperty("renewalIntervalInSecs")
    public void setRenewalIntervalInSecs(Integer renewalIntervalInSecs) {
        this.renewalIntervalInSecs = renewalIntervalInSecs;
    }

    @JsonProperty("durationInSecs")
    public Integer getDurationInSecs() {
        return durationInSecs;
    }

    @JsonProperty("durationInSecs")
    public void setDurationInSecs(Integer durationInSecs) {
        this.durationInSecs = durationInSecs;
    }

    @JsonProperty("registrationTimestamp")
    public Long getRegistrationTimestamp() {
        return registrationTimestamp;
    }

    @JsonProperty("registrationTimestamp")
    public void setRegistrationTimestamp(Long registrationTimestamp) {
        this.registrationTimestamp = registrationTimestamp;
    }

    @JsonProperty("lastRenewalTimestamp")
    public Long getLastRenewalTimestamp() {
        return lastRenewalTimestamp;
    }

    @JsonProperty("lastRenewalTimestamp")
    public void setLastRenewalTimestamp(Long lastRenewalTimestamp) {
        this.lastRenewalTimestamp = lastRenewalTimestamp;
    }

    @JsonProperty("evictionTimestamp")
    public Integer getEvictionTimestamp() {
        return evictionTimestamp;
    }

    @JsonProperty("evictionTimestamp")
    public void setEvictionTimestamp(Integer evictionTimestamp) {
        this.evictionTimestamp = evictionTimestamp;
    }

    @JsonProperty("serviceUpTimestamp")
    public Long getServiceUpTimestamp() {
        return serviceUpTimestamp;
    }

    @JsonProperty("serviceUpTimestamp")
    public void setServiceUpTimestamp(Long serviceUpTimestamp) {
        this.serviceUpTimestamp = serviceUpTimestamp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(LeaseInfo.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("renewalIntervalInSecs");
        sb.append('=');
        sb.append(((this.renewalIntervalInSecs == null)?"<null>":this.renewalIntervalInSecs));
        sb.append(',');
        sb.append("durationInSecs");
        sb.append('=');
        sb.append(((this.durationInSecs == null)?"<null>":this.durationInSecs));
        sb.append(',');
        sb.append("registrationTimestamp");
        sb.append('=');
        sb.append(((this.registrationTimestamp == null)?"<null>":this.registrationTimestamp));
        sb.append(',');
        sb.append("lastRenewalTimestamp");
        sb.append('=');
        sb.append(((this.lastRenewalTimestamp == null)?"<null>":this.lastRenewalTimestamp));
        sb.append(',');
        sb.append("evictionTimestamp");
        sb.append('=');
        sb.append(((this.evictionTimestamp == null)?"<null>":this.evictionTimestamp));
        sb.append(',');
        sb.append("serviceUpTimestamp");
        sb.append('=');
        sb.append(((this.serviceUpTimestamp == null)?"<null>":this.serviceUpTimestamp));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
