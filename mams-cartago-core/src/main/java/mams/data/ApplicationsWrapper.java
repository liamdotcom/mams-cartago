package mams.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApplicationsWrapper {

    @JsonProperty("applications")
    private Applications applications;

    public Applications getApplications() {
        return applications;
    }

    public void setApplications(Applications applications) {
        this.applications = applications;
    }

    @Override
    public String toString() {
        
        return "---> "+applications.toString();
    }
    
}
