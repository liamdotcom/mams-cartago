package mams.utils;

import java.lang.reflect.Array;
import java.util.Map;
import java.util.HashMap;


public class DefaultValue {
    /**
     * @param clazz
     *            the class for which a default value is needed
     * @return A reasonable default value for the given class (the boxed default
     *         value for primitives, <code>null</code> otherwise).
     */
    @SuppressWarnings("unchecked")
    public static <T> T forClass(Class<T> clazz) {
        return (T) DEFAULT_VALUES.get(clazz);
    }

    private static final Map<Class<?>, Object> DEFAULT_VALUES = new HashMap<Class<?>, Object>();
    private static void putValue(Class<?> clazz) {
        DEFAULT_VALUES.put(clazz, Array.get(Array.newInstance(clazz, 1), 0));
    }
    
    static {
        putValue(boolean.class);
        putValue(byte.class);
        putValue(char.class);
        putValue(double.class);
        putValue(float.class);
        putValue(int.class);
        putValue(long.class);
        putValue(short.class);
        DEFAULT_VALUES.put(String.class,"");
    }
    
    public static void main(String... args) {
        System.out.println(DefaultValue.forClass(int.class)); // 0
        System.out.println(DefaultValue.forClass(Integer.class)); // null
    }
}