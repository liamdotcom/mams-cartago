package mams.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;



/**
 * Example Usage:
 *
 * - JSON:
 *  { "name" : "rem", "address" : { "street": "rehoboth place"} , "items" : ["one", "two", "three"] }
 *
 * - ASTRA:
 *
 * import com.fasterxml.jackson.databind.JsonNode;
 *
 * agent Example {
 *   module JSONConverter converter;
 *   ...
 *   rule +!example(string json) {
 *     JsonNode node = converter.parse(json);
 *     string name = converter.valueAsString(node, "/name");
 *     string street = converter.valueAsString(node, "/address/street");
 *     string itemTwo = converter.valueAsString(node, "/address/items[1]");
 *   }
 * }
 */
public class JSONConverter {

    ObjectMapper objectMapper = new ObjectMapper();

    public JsonNode parse(String json){
        try{
            JsonNode node = objectMapper.readTree(json);
            // System.out.println("NODE: "+node);
            return node;
        } catch(JsonParseException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    //JsonNode node = json.parse(json-string)
    //json.valueAsString(node, )

    public String valueAsString(JsonNode node, String path){
        
        JsonNode jsonNode = getNode(node, path);
        return jsonNode.asText();

    }

    public JsonNode getNode(JsonNode node, String path){

        if(node == null){
            System.out.println("NODE IS NULL");
        }

        String split[] = path.split("/");
        System.out.println("SPLIT: "+split.toString());
        JsonNode temp = node;
        for(String s: split){
            if(s.isEmpty()) continue;
            System.out.println("S: "+s);
            int index  = s.indexOf("[");

            if(index == -1){
                temp = temp.get(s);
            }else{
                if(index > 0){
                    String segment = s.substring(0, index);
                    System.out.println("segment: "+segment);
                    temp = temp.get(segment); 
                }
                String rank = s.substring(index+1, s.length()-1);
                System.out.println("rank: "+rank);
                temp = temp.get(Integer.parseInt(rank));
            }
            // System.out.println("TEMP: "+temp);
        }
        return temp;
    }
}
