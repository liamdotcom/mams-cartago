package mams.utils;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;


public class HttpContext {
    private FullHttpRequest request;
    private ChannelHandlerContext ctx;
    
    public HttpContext(FullHttpRequest request, ChannelHandlerContext ctx) {
        this.request = request;
        this.ctx = ctx;
	}

    public FullHttpRequest getRequest() {
        return request;
    }

    public ChannelHandlerContext getCtx() {
        return ctx;
    }
}