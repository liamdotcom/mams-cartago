package mams.artifacts;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import cartago.OPERATION;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.util.HttpContext;
import mams.web.ResponseEntity;
import mams.web.WebServer;

public abstract class UntypedResourceArtifact extends ResourceArtifact {
    protected static ObjectMapper mapper = new ObjectMapper();
    protected Map<HttpMethod, Function<HttpContext, ResponseEntity>> methods = new HashMap<HttpMethod,Function<HttpContext, ResponseEntity>>();
    
    // protected JsonNode node;

    {
        set(HttpMethod.GET, context -> {
            try {
                return ResponseEntity.type("application/json").status(HttpResponseStatus.OK).body(mapper.writeValueAsString(getProperty("node")));
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.type("text/plain").status(HttpResponseStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        });
        set(HttpMethod.OPTIONS, context -> {
            StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (HttpMethod method : methods.keySet()) {
                if (first) first=false; else builder.append(",");
                builder.append(method.asciiName());
            }
            return new ResponseEntity(HttpResponseStatus.NO_CONTENT).header("Allow", builder.toString());
        });
        set(HttpMethod.HEAD, context -> {
            StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (HttpMethod method : methods.keySet()) {
                if (first) first=false; else builder.append(",");
                builder.append(method.asciiName());
            } 
            return new ResponseEntity(HttpResponseStatus.NO_CONTENT).header("Allow", builder.toString());
        });
    }

    protected void set(HttpMethod method, Function<HttpContext,ResponseEntity> context) {
        methods.put(method, context);
    }

    public boolean handle(ChannelHandlerContext ctx, FullHttpRequest request) {
        Function<HttpContext, ResponseEntity> method = methods.get(request.method());

        if (method == null) {
            WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.METHOD_NOT_ALLOWED);
        } else {
            WebServer.writeResponse(ctx, request, method.apply(new HttpContext(request, ctx)));
        }
        return true;
    }

	public ResourceArtifact[] getChildren() {
		return handler.getChildren();
	}

    @OPERATION
    public void init(String name, JsonNode node) {
        this.name = name;
        defineObsProperty("node", node);

    }

    @OPERATION
    public void init(String name) {
        init(name, mapper.createObjectNode());
    }

    @OPERATION public void replace(JsonNode node) {
        updateObsProperty("node", node);
        signal("update", node);
    }

    @OPERATION public void update(JsonNode node) {
        ObjectNode original = (ObjectNode) getObsProperty("node").getValue();
        node.fieldNames().forEachRemaining(field -> {
            original.set(field, node.get(field));
        });
        updateObsProperty("node", node);
        signal("update", node);
    }
}
