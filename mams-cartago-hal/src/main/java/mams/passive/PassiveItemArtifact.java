package mams.passive;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import mams.command.ItemPatchFactory;
import mams.command.ItemPutFactory;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveItemArtifact extends AbstractPassiveItemArtifact {
    {
        factories.put("PUT", new ItemPutFactory());
        factories.put("PATCH", new ItemPatchFactory());
    }
}