package astra.jena;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.RDFParser;

import astra.core.Agent;
import astra.core.Module;
import astra.event.Event;
import astra.formula.Formula;
import astra.jena.jena.event.ReadEvent;
import astra.jena.jena.event.ReadEventUnifier;
import astra.jena.jena.reasoner.TripleFormula;
import astra.jena.jena.reasoner.TripleFormulaUnifier;
import astra.jena.jena.reasoner.TripleNodeFactory;
import astra.reasoner.NewReasoner;
import astra.reasoner.Queryable;
import astra.reasoner.Unifier;
import astra.reasoner.util.AbstractEvaluateVisitor;
import astra.reasoner.util.LogicVisitor;
import astra.reasoner.util.RenameVisitor;
import astra.reasoner.util.VariableVisitor;
import astra.term.Primitive;
import astra.term.Term;
import astra.term.Variable;

public class KnowledgeStore extends Module {
	static {
        Unifier.eventFactory.put(ReadEvent.class, new ReadEventUnifier());

		NewReasoner.factories.put(TripleFormula.class, new TripleNodeFactory());
        Unifier.formulaUnifiers.add(new TripleFormulaUnifier());

		AbstractEvaluateVisitor.addFormulaHandler(new AbstractEvaluateVisitor.Handler<TripleFormula>() {
			public Class<TripleFormula> getType() { return TripleFormula.class; }
			public Object handle(LogicVisitor visitor, TripleFormula formula, boolean passByalue) {
				return new TripleFormula(
                    (Term) formula.getSubject().accept(visitor),
                    (Term) formula.getPredicate().accept(visitor),
                    (Term) formula.getObject().accept(visitor)
                );
			}
		});
		RenameVisitor.addFormulaHandler(new RenameVisitor.Handler<TripleFormula>() {
			public Class<TripleFormula> getType() { return TripleFormula.class; }
			public Object handle(LogicVisitor visitor, TripleFormula formula, String modifier, Map<Integer, Term> bindings) {
				return new TripleFormula(
                    (Term) formula.getSubject().accept(visitor),
                    (Term) formula.getPredicate().accept(visitor),
                    (Term) formula.getObject().accept(visitor)
                );
			}
		});
		VariableVisitor.addFormulaHandler(new VariableVisitor.Handler<TripleFormula>() {
			public Class<TripleFormula> getType() { return TripleFormula.class; }
			public Object handle(LogicVisitor visitor, TripleFormula formula, Set<Variable> variables) {
				formula.getSubject().accept(visitor);
				formula.getPredicate().accept(visitor);
				formula.getObject().accept(visitor);
				return null;
			}
		});
	}

    private Model model;
    private Map<String, String> namespaces = new HashMap<>();

    @Override
    public void setAgent(Agent agent) {
        super.setAgent(agent);

        model = ModelFactory.createDefaultModel();
        agent.addSource(new Queryable() {
            @Override
            @SuppressWarnings("unchecked")
            public void addMatchingFormulae(Queue<Formula> list, Formula formula) {
                if (formula instanceof TripleFormula) {
                    TripleFormula tf = (TripleFormula) formula;
                    // model.listTr
                    if (tf.getSubject() instanceof Primitive) {
                        String subject = ((Primitive<String>) tf.getSubject()).value();
                        Resource resource = model.getResource(subject);
                        Iterator<Statement> iter = resource.listProperties();
                        while (iter.hasNext()) {
                            Statement statement = iter.next();
                            list.add(new TripleFormula(
                                Primitive.newPrimitive(statement.getSubject().toString()),
                                Primitive.newPrimitive(statement.getPredicate().toString()),
                                Primitive.newPrimitive(statement.getObject().toString())
                            ));
                        }
                    } else if (tf.getPredicate() instanceof Primitive) {
                        Iterator<Statement> iter = model.listStatements();
                        while (iter.hasNext()) {
                            Statement statement = iter.next();
                            if (statement.getPredicate().toString().equals(((Primitive<String>) tf.getPredicate()).value())) {
                                list.add(new TripleFormula(
                                    Primitive.newPrimitive(statement.getSubject().toString()),
                                    Primitive.newPrimitive(statement.getPredicate().toString()),
                                    Primitive.newPrimitive(statement.getObject().toString())
                                ));
                            }
                        }
                    } else {
                        System.out.println("BANG!");
                    }
                }
            }

            @Override
            public Iterator<Formula> iterator(Formula target) {
                if (target instanceof TripleFormula) {
                    System.out.println("++++++++++++++++++++++ BAH HUMBUG ++++++++++++++++++++++");
                }
                
                return null;
            }
            
        });
    }
    
    /**
     * Legacy method - was used in the Maze problem, so keeping it for now.
     * Should use getKnowledge(...) instead.
     * @deprecated
     * @param url
     * @return
     */
    @ACTION
    public boolean readUrl(String url) {
        return getKnowledge(url, "text/turtle");
    }

    @ACTION
    public boolean getKnowledge(String url) {
        return getKnowledge(url, "text/turtle");
    }

    @ACTION
    public boolean getKnowledge(String url, String mediaType) {
        RDFParser.create()
            .source(url)
            .httpAccept(mediaType)
            .parse(model);

        agent.addEvent(new ReadEvent(Primitive.newPrimitive(url)));
        return true;
    }

    @ACTION
    public boolean addSchema(String name, String uri) {
        namespaces.put(name, uri);
        return true;
    }

    @ACTION
    public boolean displayModel() {
        Iterator<Statement> iterator = model.listStatements();

        while (iterator.hasNext()) {
            Statement stmt = iterator.next();
            Resource subject = stmt.getSubject();
            Property predicate = stmt.getPredicate();
            RDFNode object = stmt.getObject();

            System.out.print(subject.toString());
            System.out.print(" " + predicate + " ");
            if (object instanceof Resource) {
                System.out.print(object.toString());
            } else {
                // object is a literal
                System.out.print(" \"" + object.toString() + "\"");
            }

            System.out.println(" .");
        }
        return true;        
    }

    @ACTION
    public boolean clear() {
        model.removeAll();
        return true;
    }
    
    @TERM
    public String lf(String name, String value) {
        return namespaces.get(name) + value;
    }

    @FORMULA(types={"string", "string", "string"})
    public Formula triple(Term subject, Term predicate, Term object) {
        return new TripleFormula(subject, predicate, object);
    }

    @EVENT(types={"string"}, signature = "$re")
    public Event read(Term url) {
        return new ReadEvent(url);
    }    
}