package astra.jena;

import astra.core.Module;

public class XMLLiteralParser extends Module {
    
    @TERM
    public int intValue(String value) {
        if (value.endsWith("http://www.w3.org/2001/XMLSchema#integer")) {
            return Integer.parseInt(value.substring(0, value.indexOf("^^")));
        }
        throw new RuntimeException("Attempt to Parse invalid XML Literal: " +value);
    }

    @TERM
    public double doubleValue(String value) {
        if (value.endsWith("http://www.w3.org/2001/XMLSchema#double")) {
            return Double.parseDouble(value.substring(0, value.indexOf("^^")));
        }
        throw new RuntimeException("Attempt to Parse invalid XML Literal: " +value);
    }

    @TERM
    public String asString(String value){
        return "\"" +value+ "\"";
    }
}
