package astra.jena;

import astra.core.Module;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;

public class TurtleWriter extends Module {
    @TERM
    @SuppressWarnings("unchecked")
    public String write(String base, ListTerm triples) {
        StringBuilder builder = new StringBuilder();

        builder.append("@base <").append(base).append("> .\n\n");

        for (Term term : triples.terms()) {
            Funct triple = (Funct) term;
            if (triple.functor().equals("triple")) {
                builder.
                    append(((Primitive<String>) triple.termAt(0)).value()).
                    append(" ").
                    append(((Primitive<String>) triple.termAt(1)).value()).
                    append(" ").
                    append(((Primitive<String>) triple.termAt(2)).value()).
                    append(" .\n");
            }
        }

        return builder.toString();
    }
}
