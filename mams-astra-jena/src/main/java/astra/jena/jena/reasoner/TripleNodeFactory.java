package astra.jena.jena.reasoner;

import java.util.Map;

import astra.reasoner.node.ReasonerNode;
import astra.reasoner.node.ReasonerNodeFactory;
import astra.term.Term;

public class TripleNodeFactory implements ReasonerNodeFactory<TripleFormula> {

    @Override
    public ReasonerNode create(TripleFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new TripleNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, TripleFormula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new TripleNode(parent, formula, bindings, singleResult);
    }
    
}
