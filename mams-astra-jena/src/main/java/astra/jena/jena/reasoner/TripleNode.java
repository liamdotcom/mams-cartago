package astra.jena.jena.reasoner;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import astra.formula.Formula;
import astra.reasoner.Queryable;
import astra.reasoner.Reasoner;
import astra.reasoner.Unifier;
import astra.reasoner.node.ReasonerNode;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public class TripleNode extends ReasonerNode {
    TripleFormula formula;
	Queue<Formula> options = new LinkedList<>();
    int state = 0;

    public TripleNode(ReasonerNode parent, TripleFormula formula, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.formula = formula;
        this.initial = initial;
    }

    @Override
    public ReasonerNode initialize(Reasoner reasoner) {
        visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        formula = (TripleFormula) formula.accept(visitor);
        for (Queryable source : reasoner.sources()) {
            source.addMatchingFormulae(options, formula);
        }
        return super.initialize(reasoner);
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        while (!options.isEmpty()) {
            Formula f = options.poll();
            Map<Integer, Term> bindings = Unifier.unify(formula, (TripleFormula) f.accept(visitor), new HashMap<Integer, Term>(initial), reasoner.agent());
            if (bindings != null) {
                solutions.add(bindings);
                if (singleResult) {
                    finished = true;
                }
                return true;
            }
        }

        finished = true;
        return !(failed = solutions.isEmpty());
    }

    
}
