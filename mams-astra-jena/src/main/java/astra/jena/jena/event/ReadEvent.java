package astra.jena.jena.event;

import astra.event.Event;
import astra.reasoner.util.LogicVisitor;
import astra.term.Term;

public class ReadEvent implements Event {
	Term url;

	public ReadEvent(Term url) {
		this.url = url;
	}

	public Term url() {
		return url;
	}
	@Override
	public Object getSource() {
		return null;
	}

	@Override
	public String signature() {
		return "$re";
	}

	@Override
	public Event accept(LogicVisitor visitor) {
		return new ReadEvent((Term) url.accept(visitor));
	}
    
}