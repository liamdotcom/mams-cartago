package astra.jena;

import astra.core.Module;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.jena.jena.reasoner.TripleFormula;
import astra.term.Primitive;

public class RDFSchema extends Module {
    private String url;

    public RDFSchema(String url) {
        this.url = url;
        // System.out.println("Setting URL: " + url);
    }

    @TERM
    public String qualifiedName(String local) {
        return url + local;
    }

    @TERM
    public String qualifiedTurtle(String local) {
        return "<"+url + local+">";
    }

    @TERM
    public String localName(String qualified) {
        return qualified.substring(url.length());
    }

    @FORMULA
    public Formula contains(String qName) {
        return qName.startsWith(url) ? Predicate.TRUE:Predicate.FALSE;
    }

    @FORMULA
    public Formula auto_formula(Predicate predicate) {
        if (predicate.size() != 2) return Predicate.FALSE;

        return new TripleFormula(predicate.getTerm(0), Primitive.newPrimitive(url+predicate.predicate()), predicate.getTerm(1));
    }
}
